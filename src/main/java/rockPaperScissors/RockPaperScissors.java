package rockPaperScissors;

import java.util.*;

public class RockPaperScissors {

    public static void main(String[] args) {
        /*
         * The code here does two things:
         * It first creates a new RockPaperScissors -object with the
         * code `new RockPaperScissors()`. Then it calls the `run()`
         * method on the newly created object.
         */
        new RockPaperScissors().run();
    }


    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    String humanChoice;


    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next().toLowerCase();
        return userInput;
    }


    public boolean is_winner(String humanChoice, String computerChoice) {
        if (humanChoice.equals("paper")) {
            return (computerChoice.equals("rock"));
        } else if (humanChoice.equals("scissors")) {
            return (computerChoice.equals("paper"));
        } else {
            return (computerChoice.equals("scissors"));
        }
    }

    public String userChoice(String humanChoice) {
        // while (true) {
        if (rpsChoices.contains(humanChoice)) {
            // break;
            return humanChoice;
        } else {
            String answerBack = ("I dont understand" + humanChoice + ". Try again");
            System.out.println(answerBack);
            // continue;
            return answerBack;
        }
    }

    public String continuePlaying() {
        // while (true) {
        String continueAnswer = readInput("Do you wish to continue playing? (y/n)?");
        if (continueAnswer.equals("y") || continueAnswer.equals("n")) {
            return continueAnswer;
        } else {
            String answerBack = ("I dont understand" + continueAnswer + ". Try again");
            return answerBack;

        }
    }


    public void run() {
        while (true) {
            System.out.println("Let's play round " + roundCounter);
            humanChoice = readInput("Your choice (Rock/Paper/Scissors)? ");

            if (rpsChoices.contains(humanChoice)) {
                Random rand = new Random();
                String computerChoice = rpsChoices.get(rand.nextInt(rpsChoices.size()));


                if (is_winner(humanChoice, computerChoice)) {
                    System.out.println("Human chose " + humanChoice + ", " + "computer chose " + computerChoice + ". Human wins!");
                    humanScore++;

                } else if (is_winner(computerChoice, humanChoice)) {
                    System.out.println("Human chose " + humanChoice + ", " + "computer chose " + computerChoice + ". Computer wins!");
                    computerScore++;

                } else{
                    System.out.println("Human chose " + humanChoice + ", " + "computer chose " + computerChoice + ". It's a tie!");

                }
                String scoreBoth = ("Score: human " + humanScore + " computer " + computerScore);
                System.out.println(scoreBoth);
                roundCounter++;
                String continueAnswer = continuePlaying();

                if (continueAnswer.equals("n")) {
                    System.out.println("Bye bye :)");
                    break;
                }
            }
            else {
                System.out.println("I dont understand " + humanChoice + ". Try again");


            }
        }
    }
}
